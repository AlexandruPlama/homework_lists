
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> list1 = new LinkedList<>();
        list1.add("Green");
        list1.add("Red");
        list1.add("Blue");
        System.out.println(list1);

        list1.add(0,"Yellow");
        System.out.println(list1);

        System.out.println(list1.size());

        Collections.sort(list1);
        System.out.println(list1);

        System.out.println(list1.contains("Blue"));

        List<String> list2 = new ArrayList<>();
        list2.add("White");
        list2.add("Black");

        System.out.println(list1.containsAll(list2));

        list1.addAll(list2);
        System.out.println(list1);

        list1.remove("Red");
        System.out.println(list1);

        for(String i : list1)
            System.out.println(i);
    }
}
