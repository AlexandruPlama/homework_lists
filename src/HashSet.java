import java.util.LinkedHashSet;
import java.util.Set;

public class HashSet {
    public static void main(String[] args) {
        Set<String> set1 = new LinkedHashSet<>();
        set1.add("Green");
        set1.add("Red");
        set1.add("Blue");
        System.out.println(set1);

        set1.add("Yellow");
        set1.add("Green");
        System.out.println(set1);

        System.out.println(set1.size());

        System.out.println(set1.contains("Blue"));

        Set<String> set2 = new LinkedHashSet<>();
        set1.add("White");
        set1.add("Black");

        System.out.println(set1.containsAll(set2));

        set1.addAll(set2);
        System.out.println(set1);

        set1.remove("Red");
        System.out.println(set1);

        for(String i : set1)
            System.out.println(i);
    }
}
